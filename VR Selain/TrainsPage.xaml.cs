﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Input;
using System.IO;
using System.Xml;
using System.Globalization;

namespace VR_Selain
{
    public partial class TrainsPage : PhoneApplicationPage
    {
        public TrainsPage()
        {
            InitializeComponent();
        }
        private class Train
        {
            public string Id { get; set; }
            public string Title { get; set; }
            public string From { get; set; }
            public string To { get; set; }
        }
  

        private List<Train> trains;
        private WebClient client;



        private void SearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            SearchBox.Text = "";
            
        }

        private void SearchBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Focus();
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //TODO: SEARCH LIST
            // TODO2
            if (trains == null || trains.Count == 0)
            {
                /*too late*/
                return;
            }
            if (SearchBox.Text.Length != 0 && trains != null)
            {

                //TODO: return trains
                string temp = SearchBox.Text;
                temp = temp.ToUpper();
                temp = temp.Trim();
                updateTrainsList(temp);
            }
            else
            {
                TrainsListBox.ItemsSource = trains;
            }


        }

        private void updateTrainsList(string str)
        {
            List<Train> trainsFound = new List<Train>();
            foreach(Train trn in trains)
            {

                if (trn.Title.Contains(str) || (trn.From.ToUpper()).Contains(str) || (trn.To.ToUpper()).Contains(str))
                {
                    trainsFound.Add(trn);
                }
            }
            TrainsListBox.ItemsSource = trainsFound;
        }
                


        /* XML Content parsing*/
        private static string TRAINS_URL = "http://188.117.35.14/TrainRSS/TrainService.svc/AllTrains";
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            client = new WebClient();
            client.DownloadStringAsync(new Uri(TRAINS_URL));
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_downloadCompleted);
        }

        void client_downloadCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            trains = new List<Train>();

            if (e.Error == null && !e.Cancelled)
            {
                StringReader str = new StringReader(e.Result);
                XmlReader reader = XmlReader.Create(str);
                if (reader.CanResolveEntity)
                {
                    Train train;
                    reader.ReadToFollowing("item");
                    while (reader.Read())
                    {
                        train = new Train();

                        if (reader.ReadToFollowing("guid"))
                        {
                            train.Id = reader.ReadElementContentAsString();

                        }
                        else
                        {
                            train.Id = "";
                        }
                        if (reader.ReadToFollowing("title"))
                        {
                            train.Title = reader.ReadElementContentAsString();
                        }
                        else
                        {
                            train.Title = "";
                        }

                        if (reader.ReadToFollowing("from"))
                        {
                            /*Read the id as real name HKI => HELSINKI*/
                            train.From = WelcomePage.getNameFromId(reader.ReadElementContentAsString());
                            train.To = WelcomePage.getNameFromId(reader.ReadElementContentAsString());
                        }
                        

                        trains.Add(train);
                    }
                    if (trains.Count != 0 && trains.Last().Title == "")
                    {
                        trains.RemoveAt(trains.Count() - 1);
                    }
                    TrainsListBox.ItemsSource = trains;
                }
            }
        }
    }
}