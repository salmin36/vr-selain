﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml;
namespace VR_Selain
{
    public partial class MainPage : PhoneApplicationPage
    {
        public List<Asema> perkl;
        public class Asema
        {
            public String Id { get; set; }
            public String Name { get; set; }
        }
        // Constructor
        public MainPage()
        {
            InitializeComponent();   
        }
   


        private void loadAsema(object sender, RoutedEventArgs e)
        {
            Asema asem = (Asema)((Button)sender).DataContext;
            this.NavigationService.Navigate(new Uri("/InfoPage.xaml?id="+asem.Id + "&name="+asem.Name, UriKind.Relative));
            //MessageBox.Show("Button Clicked and it contains: " + asem.Name);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            perkl = new List<Asema>();
            XmlReader reader = XmlReader.Create("asemakoodit.xml");
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "asema")
                {
                    if (reader.HasAttributes)
                    {
                        perkl.Add(new Asema() { Id = reader.GetAttribute("id"), Name = reader.ReadElementContentAsString() });
                        //Console.WriteLine("Aseman koodi: " + reader.GetAttribute("id") + " Aseman nimi: " + reader.ReadElementContentAsString());
                    }
                }
            }
            AsemaLista.ItemsSource = perkl;
        }

    }
}