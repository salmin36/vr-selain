﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Xml;

namespace VR_Selain
{
    public partial class WelcomePage : PhoneApplicationPage
    {
        public class Station
        {
            public String Id { get; set; }
            public String Name { get; set; }
        }
        public static List<Station> stationList;
        public WelcomePage()
        {
            InitializeComponent();

            stationList = new List<Station>();
            XmlReader reader = XmlReader.Create("asemakoodit.xml");
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "asema")
                {
                    if (reader.HasAttributes)
                    {
                        stationList.Add(new Station() { Id = reader.GetAttribute("id"), Name = reader.ReadElementContentAsString() });
                        //Console.WriteLine("Train koodi: " + reader.GetAttribute("id") + " Trainn nimi: " + reader.ReadElementContentAsString());
                    }
                }
            }
        }

        public static List<Station> getStations() 
        {
            return stationList;
        }

        public static String getNameFromId(String Id)
        {
            foreach (Station element in stationList)
            {
                if (element.Id == Id)
                {
                    return element.Name;
                }
            }
            System.Diagnostics.Debug.WriteLine("Ei löytynyt id:tä " + Id + " vastaavaa nimeä");
            return "";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}