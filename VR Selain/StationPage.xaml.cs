﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Device.Location;
using Microsoft.Phone.Controls.Maps;
using System.IO;
using System.Xml;

namespace VR_Selain
{
    public partial class StationPage : PhoneApplicationPage
    {
        public class Train
        {
            public string Guid { get; set; }
            public string Name { get; set; }
            public string Etd { get; set; }
            public string ToStation { get; set; }
            public string Color { set; get; }
            public string Delay { set; get; }
        }

        private List<Train> trains;
        private WebClient client;

        
        private string position;

        public static string ASEMA_URL = "http://188.117.35.14/TrainRSS/TrainService.svc/stationInfo?station=";

        public StationPage()
        {
            InitializeComponent();

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            StationName.Header = (this.NavigationContext.QueryString["name"]);
            /**/
            base.OnNavigatedTo(e);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            trains = new List<Train>();
            string url = ASEMA_URL + this.NavigationContext.QueryString["id"];


            client = new WebClient();
            client.DownloadStringAsync(new Uri(url));

            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_downloadCompleted);
        }

        void client_downloadCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled)
            {
                StringReader str = new StringReader(e.Result);
                if(str.Peek() == -1)
                {
                    System.Diagnostics.Debug.WriteLine("XML-streamissä ei mitään");

                    return;
                }
                XmlReader reader = XmlReader.Create(str);

                if (reader.CanResolveEntity)
                {
                                        //reader.ReadToFollowing("title");
                    //stationName = reader.ReadElementContentAsString();

                    if (!reader.ReadToFollowing("georss:point"))
                    {
                        return;
                    }
                    position = reader.ReadElementContentAsString();

                    reader.ReadToFollowing("item");
                    Train added;
                    string str1 = position;
                    int index = str1.IndexOf(' ');
                    str1.Substring(0, index);
                    double longitude, latitude;
                    if (double.TryParse(str1.Substring(0, index), out latitude))
                    {
                        if (double.TryParse(str1.Substring(index + 1), out longitude))
                        {
                            GeoCoordinate coordinates = new GeoCoordinate(latitude, longitude);

                            trainMap.SetView(coordinates, 14);
                            Pushpin station = new Pushpin();


                            station.Location = new GeoCoordinate(latitude, longitude);
                            trainMap.Children.Add(station);
                            trainMap.IsEnabled = false;

                        }
                    }


                  

                    while (reader.Read())
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "guid"))
                        {
                            added = new Train();
                            added.Guid = reader.ReadElementContentAsString();
                            if (reader.ReadToNextSibling("title"))
                            {
                                added.Name = reader.ReadElementContentAsString();
                            }

                            if (reader.ReadToNextSibling("etd"))
                            {
                                added.Etd = reader.ReadElementContentAsString();
                            }
                            else
                            {
                                added.Etd = "";
                            }

                            if (reader.ReadToNextSibling("toStation"))
                            {
                                added.ToStation = WelcomePage.getNameFromId(reader.ReadElementContentAsString());
                            }
                            else
                            {
                                added.ToStation = "";
                            }
                            if (reader.ReadToNextSibling("lateness"))
                            {
                                double temp = reader.ReadElementContentAsDouble()/60;
                                if (temp != 0)
                                {
                                    added.Delay = "+" + temp.ToString();
                                }
                                else 
                                {
                                    added.Delay = "";
                                }
                            }
                            else
                            {
                                added.Delay = "";
                            }
                            added.Color = "LimeGreen";
                            trains.Add(added);

                        }
                    }
                }
            }
            /* if (junat.Count > 0 && junat.First().Name == pageName.Text) 
             {
                 junat.RemoveAt(0);
             }*/
            TrainsListBox.ItemsSource = trains;
        }
    }
}