﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Xml;
using System.IO;

namespace VR_Selain
{
    public partial class InfoPage : PhoneApplicationPage
    {
        public class Juna
        {
            public string Name { get; set;}
            public string Eta { get; set; }
            public string Etd { get; set; }
            public string ToStation { get; set; }
            
        }
       
        private List<Juna> junat;
        private WebClient client;
        private string position;

        public InfoPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            pageName.Text = (this.NavigationContext.QueryString["name"]);
            base.OnNavigatedTo(e);
        }
        

        public static string ASEMA_URL = "http://188.117.35.14/TrainRSS/TrainService.svc/stationInfo?station=";

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            junat = new List<Juna>();
            string url = ASEMA_URL + this.NavigationContext.QueryString["id"];
            

            client = new WebClient();
            client.DownloadStringAsync(new Uri(url));

            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_downloadCompleted);

            
            
        }

        void client_downloadCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled)
            {
                StringReader str = new StringReader(e.Result);
                XmlReader reader = XmlReader.Create(str);
                
                if (reader.CanResolveEntity)
                {
                    reader.ReadToFollowing("georss:point");
                    position = reader.ReadElementContentAsString();
                    
                    reader.ReadToFollowing("item");
                    Juna added;
                    while (reader.Read())
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "title"))
                        {
                            added = new Juna();
                            added.Name = reader.ReadElementContentAsString();
                            if (reader.ReadToNextSibling("eta"))
                            {
                                added.Eta = reader.ReadElementContentAsString();
                            }
                            else 
                            {
                                added.Eta = "";
                            }


                            added.Etd = reader.ReadElementContentAsString();
                            
                            
                            if (reader.ReadToNextSibling("toStation"))
                            {
                                added.ToStation = reader.ReadElementContentAsString();
                            }
                            else
                            {
                                added.ToStation = "";
                            }
                            
                            junat.Add(added);
                            
                        }
                    }
                }
            }
           /* if (junat.Count > 0 && junat.First().Name == pageName.Text) 
            {
                junat.RemoveAt(0);
            }*/
            Junalista.ItemsSource = junat;
        }

        private void Junalista_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {/*
            Juna juna = (sender as ListBox).SelectedItem as Juna;
            ListBoxItem selectedItem = this.Junalista.ItemContainerGenerator.ContainerFromItem(juna) as ListBoxItem;
            MessageBox.Show("GPS: ");*/
        }

        private void infoPage_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/MapsPage.xaml?gps="+position+"&name="+pageName.Text, UriKind.Relative));
        }
    }
}