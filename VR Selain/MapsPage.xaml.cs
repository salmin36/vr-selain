﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Device.Location;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Controls.Maps;

namespace VR_Selain
{
    public partial class MapsPage : PhoneApplicationPage
    {
        public MapsPage()
        {
            InitializeComponent();
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            pageName.Text = (this.NavigationContext.QueryString["name"]);
            string str = (this.NavigationContext.QueryString["gps"]);
            int index = str.IndexOf(' ');
            str.Substring(0, index);
            double longitude, latitude;
            if (double.TryParse(str.Substring(0, index), out latitude)) 
            {
                if (double.TryParse(str.Substring(index + 1), out longitude))
                {
                    GeoCoordinate coordinates = new GeoCoordinate(latitude, longitude);
                    //LocationRect.CreateLocationRect();
                    trainMap.SetView(coordinates, 10);
                    
                }
            }
 
            base.OnNavigatedTo(e);
        }
    }
}