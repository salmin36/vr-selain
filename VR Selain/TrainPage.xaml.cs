﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace VR_Selain
{
    public partial class TrainPage : PhoneApplicationPage
    {
        public TrainPage()
        {
            InitializeComponent();
        }
        public class Station
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Eta { get; set; }
            public string Etd { get; set; }
            public int Completed { get; set; }
            public string color { get; set; }
        }

        public List<Station> stations;
        private WebClient client;
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}