﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml;
namespace VR_Selain
{
    public partial class StationsPage : PhoneApplicationPage
    {
        // Constructor
        public StationsPage()
        {
            InitializeComponent();   
        }
        private void loadStation(object sender, RoutedEventArgs e)
        {
            VR_Selain.WelcomePage.Station asem = (VR_Selain.WelcomePage.Station)((Button)sender).DataContext;
            this.NavigationService.Navigate(new Uri("/StationPage.xaml?id=" + asem.Id + "&name=" + asem.Name, UriKind.Relative));
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            //Tähän parsetus
            StationsListBox.ItemsSource = WelcomePage.getStations();
        }
    }
}